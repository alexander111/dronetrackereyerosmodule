#include "droneTrackerEyeROSModule.h"

DroneTrackerEyeROSModule::DroneTrackerEyeROSModule():
    DroneModule(droneModule::active, DRONETRACKEREYEROSMODULE_TLD_EXPECTED_FPSCHANNEL_RATE)
{
    init();
    std::cout << "DroneTrackerEyeROSModule(..), enter and exit " << std::endl;

    fps_thread=new std::thread(&DroneTrackerEyeROSModule::fps_thread_main,this);

    return;
}

DroneTrackerEyeROSModule::~DroneTrackerEyeROSModule()
{
    init();
}

void DroneTrackerEyeROSModule::init()
{
    bounding_box.x          = 0;
    bounding_box.y          = 0;
    bounding_box.width      = 0;
    bounding_box.height     = 0;
    bounding_box.confidence = 0.0;
    fps.data = -1.0;

    tracking_object         = false;
    is_object_on_frame      = false;
    received_fps_atleast_once = false;

    last_time_recieved_fps = ros::Time::now();
    last_time_recieved_bb  = ros::Time::now();

    return;
}


void DroneTrackerEyeROSModule::close()
{
    return;
}

bool DroneTrackerEyeROSModule::resetValues()
{

    if(!DroneModule::resetValues())
        return false;


    init();


    // Publish flags
    publishTrackingObject();
    publishIsObjectOnFrame();


    return true;
}



bool DroneTrackerEyeROSModule::startVal()
{
    if(!DroneModule::startVal())
        return false;

    return true;
}

bool DroneTrackerEyeROSModule::stopVal()
{
    if(!DroneModule::stopVal())
        return false;

    resetValues();
    return true;
}

bool DroneTrackerEyeROSModule::run()
{
     if(!DroneModule::run())
         return false;

     return true;
}


void DroneTrackerEyeROSModule::open(ros::NodeHandle & nIn)
{


     DroneModule::open(nIn);

     //Subscriber names
     ros::param::get("~Open_tld_translator_tracked_object_topic_name", Open_tld_translator_tracked_object_topic_name);
     if( Open_tld_translator_tracked_object_topic_name.length() == 0)
     {
         Open_tld_translator_tracked_object_topic_name="Open_tld_translator/tracked_object";
     }
     ros::param::get("~Open_tld_translator_fps_topic_name", Open_tld_translator_fps_topic_name);
     if( Open_tld_translator_fps_topic_name.length() == 0)
     {
         Open_tld_translator_fps_topic_name="Open_tld_translator/fps";
     }

     //Publisher Names
     ros::param::get("~tracking_object_topic_name", tracking_object_topic_name);
     if( tracking_object_topic_name.length() == 0)
     {
         tracking_object_topic_name="tracking_object";
     }
     ros::param::get("~is_object_on_frame_topic_name", is_object_on_frame_topic_name);
     if( is_object_on_frame_topic_name.length() == 0)
     {
         is_object_on_frame_topic_name="is_object_on_frame";
     }
     ros::param::get("~get_bounding_box_topic_name", get_bounding_box_topic_name);
     if( get_bounding_box_topic_name.length() == 0)
     {
         get_bounding_box_topic_name="get_bounding_box";
     }


      //Subscribers
      bounding_box_sub    = n.subscribe(Open_tld_translator_tracked_object_topic_name, 1, &DroneTrackerEyeROSModule::boundingBoxSubCallback, this);
      fps_sub             = n.subscribe(Open_tld_translator_fps_topic_name, 1, &DroneTrackerEyeROSModule::fpsSubCallback, this);

      //publishers
      tracking_object_pub     = n.advertise<std_msgs::Bool>(tracking_object_topic_name,1,true);
      is_object_on_frame_pub  = n.advertise<std_msgs::Bool>(is_object_on_frame_topic_name,1,true);
      get_bounding_box_pub    = n.advertise<droneMsgsROS::BoundingBox>(get_bounding_box_topic_name,1,true);


      droneModuleOpened = true;

    return;
}


void DroneTrackerEyeROSModule::fps_thread_main()
{
    std::cout<<"fps thead inited"<<std::endl;

    while(ros::ok())
    {
        ros::spinOnce();

        bool isTrackingObjectFlag=isTrackingObject();
        bool isObjectOnFrameFlag=isObjectOnFrame();

        if(!isTrackingObjectFlag || !isObjectOnFrameFlag)
        {
            if(moduleStarted)
            {
                // Publish flags
                publishTrackingObject();
                publishIsObjectOnFrame();

                // Publish BB only if is on frame
                getBoundingBox();
            }
        }
        sleep();

    }


    return;
}


void DroneTrackerEyeROSModule::boundingBoxSubCallback(const droneMsgsROS::BoundingBox::ConstPtr &msg)
{
    if(!moduleStarted)
        return;

//    std::cout << "DroneTrackerEyeROSModule::boundingBoxSubCallback()" << std::endl;


    bounding_box = (*msg);


    // modified to comply with this ros_opentld commit:
    //   https://github.com/Ronan0912/ros_opentld/commit/f07234fee2a3a6a0373a4312163e63d2d935135c
    if ( (msg->height==1) && (msg->width==1) &&
         (msg->x     ==1) && (msg->y    ==1) )
    {
        //std::cout<<"Object out of frame."<<std::endl;

        is_object_on_frame = false;
        tracking_object=true;

    }
    else if ( (msg->height==0) && (msg->width==0) &&
             (msg->x     ==0) && (msg->y    ==0) )
    {
        //std::cout<<"Object reseted."<<std::endl;

        is_object_on_frame = false;
        tracking_object=false;
    }
    else
    {
        //std::cout<<"Object on frame."<<std::endl;

        tracking_object=true;
        is_object_on_frame = true;

        // updating OpenTLD status information
        last_time_recieved_bb = ros::Time::now();

    }

    // Publish flags
    publishTrackingObject();
    publishIsObjectOnFrame();

    // Publish BB only if is on frame
    getBoundingBox();

    if(!run())
        return;

    return;


}

void DroneTrackerEyeROSModule::fpsSubCallback(const std_msgs::Float32::ConstPtr &msg)
{
    if(!moduleStarted)
        return;

//    std::cout << "DroneTrackerEyeROSModule::fpsSubCallback()" << std::endl;
    fps = (*msg);

    // updating OpenTLD status information
    received_fps_atleast_once = true;
    last_time_recieved_fps = ros::Time::now();


    if(!run())
        return;


    return;
}

bool DroneTrackerEyeROSModule::isTrackingObject()
{

    // updating OpenTLD status information
    ros::Time current_time = ros::Time::now();
    ros::Duration elapsed_time = current_time - last_time_recieved_fps;

    if (received_fps_atleast_once)
        if ( elapsed_time.toSec() > 5.0*(1.0/DRONETRACKEREYEROSMODULE_TLD_EXPECTED_FPSCHANNEL_RATE) )
            tracking_object = false;
        else
            tracking_object = true;
    else
        tracking_object = false;


    return tracking_object;
}

bool DroneTrackerEyeROSModule::isObjectOnFrame()
{
    if (!tracking_object)
        is_object_on_frame = false;

    ros::Time current_time = ros::Time::now();
    ros::Duration elapsed_time = current_time - last_time_recieved_bb;

    if ( elapsed_time.toSec() > 3.0*(25.0/15.0)*(1.0/DRONETRACKEREYEROSMODULE_TLD_EXPECTED_FPSCHANNEL_RATE) )
        is_object_on_frame = false;


    return is_object_on_frame;
}

bool DroneTrackerEyeROSModule::getBoundingBox()
{
    if (is_object_on_frame && tracking_object && (bounding_box.width*bounding_box.height > 0))
    {
        // Do nothing
    }
    else
    {
        bounding_box.x          = 0;
        bounding_box.y          = 0;
        bounding_box.width      = 0;
        bounding_box.height     = 0;
        bounding_box.confidence = 0.0;

    }
    publishGetBoundingBox();

    return true;
}

void DroneTrackerEyeROSModule::publishTrackingObject()
{
    tracking_object_tbp.data = tracking_object;
    tracking_object_pub.publish(tracking_object_tbp);
    return;
}

void DroneTrackerEyeROSModule::publishIsObjectOnFrame()
{
    is_object_on_frame_tbp.data = is_object_on_frame;
    is_object_on_frame_pub.publish(is_object_on_frame_tbp);
    return;
}

void DroneTrackerEyeROSModule::publishGetBoundingBox()
{
    get_bounding_box_tbp.x          = bounding_box.x;
    get_bounding_box_tbp.y          = bounding_box.y;
    get_bounding_box_tbp.width      = bounding_box.width;
    get_bounding_box_tbp.height     = bounding_box.height;
    get_bounding_box_tbp.confidence = bounding_box.confidence;

    get_bounding_box_pub.publish(get_bounding_box_tbp);
    return;
}


void DroneTrackerEyeROSModule::print()
{
    std::cout << "DroneTrackerEyeROSModule::print() " << std::endl;
    // OpenTLD status information
    std::cout << "tracking_object:" << (isTrackingObject() ? "True" : "False");
    std::cout << " is_object_on_frame:" << (is_object_on_frame ? "True" : "False") << std::endl;
    std::cout << "last tld_msgs::BoundingBox:" << std::endl;
    std::cout << bounding_box;
    std::cout << "last tld_fps:" << fps.data << std::endl;
}
